<?php
declare(strict_types=1);

use App\Http\Controllers\OriginsController;
use App\Http\Controllers\PingController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['register' => false]);

Route::get('/', function () {
    return view('welcome');
});

Route::get('/ping/{uuid?}', [PingController::class, 'check']);

Route::middleware('auth')->group(function () {
    Route::get('/origins', [OriginsController::class, 'list']);
});
