# CDC

## Fonctionnement

### Administration

1. Créer un client d'hébergement.
2. Ajouter un projet au client :
    
    - Renseigner le titre du projet.
    - Ajouter le nom de domain <small>(ou sous-domain)</small>.
    - Ajouter l'adresse du dépôt de code source.
    - Copier le _token_ auto-généré et insérer dans les _hooks_ pour les _push_ du dépôt de code source.

    
### API

#### GIT

1. Appel à l'API à l'adresse `/ping` avec le _token_ qui est émis lors d'un _commit_.
2. L'application vérifie l'authenticité du _token_.
3. Une tâche est mise en _queue_ pour lancer un : 
        
    - _clone_ du dépôt git dans le dossier.
    - _pull_ du dernier _commit_.
    
4. Un _event_ est émis.


#### Markdown

1. Le compilateur est sollicité par son _listener_ qui répond à l'_event_ de la tâche [git](#git).
2. Le site est construit et sauvegardé dans le dossier public


### Front

1. Accès au nom de domaine.
2. Le site est présenté sous sa forme définitive.


## Technique

Les bibliothèques suivantes sont utilisés :

- Laravel Sanctum
- 
