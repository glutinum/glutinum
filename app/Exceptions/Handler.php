<?php
declare(strict_types=1);

namespace App\Exceptions;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Throwable;

class Handler extends ExceptionHandler
{

    /**
     * @param  Request  $request
     * @param  Exception  $exception
     *
     * @return Application|JsonResponse|RedirectResponse|Response|Redirector|\Symfony\Component\HttpFoundation\Response
     * @throws Throwable
     */
    public function render($request, $exception)
    {
        if ($exception instanceof MissingKeyException) {
            return response(__("Missing key for ping."), 400);
        }

        return parent::render($request, $exception);
    }

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
