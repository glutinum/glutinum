<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\MissingKeyException;
use Illuminate\Http\Response;
use Ramsey\Uuid\Uuid;

class PingController extends Controller
{

    /**
     * @param  string|null  $uuid
     *
     * @return Response
     * @throws MissingKeyException
     */
    public function check(?string $uuid = null): Response
    {
        if (Uuid::isValid($uuid ?? '')) {
            return response(null);
        } else {
            throw new MissingKeyException();
        }
    }

}
