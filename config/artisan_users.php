<?php
declare(strict_types=1);

return [
    'use_model' => App\Models\User::class,
    'with_roles' => true,
];
