# Glutinum

[![Latest Version on Packagist](https://img.shields.io/packagist/v/glutinum/glutinum.svg?style=flat-square)](https://packagist.org/packages/glutinum/glutinum)
[![Build Status](https://img.shields.io/travis/glutinum/glutinum/master.svg?style=flat-square)](https://travis-ci.org/glutinum/glutinum)
[![Quality Score](https://img.shields.io/scrutinizer/g/glutinum/glutinum.svg?style=flat-square)](https://scrutinizer-ci.com/g/glutinum/glutinum)
[![Total Downloads](https://img.shields.io/packagist/dt/glutinum/glutinum.svg?style=flat-square)](https://packagist.org/packages/glutinum/glutinum)

A Laravel project allowing for the compilation of markdown pages to static documentation websites.


### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email marc-andre@appel.fun instead of using the issue tracker.

## Credits

- [Marc-André Appel](https://github.com/glutinum)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
