<?php
declare(strict_types=1);

use Faker\Provider\Uuid;

uses()->group('ping');

it('does accept a ping with an UUID', function () {
    $response = $this->get('/ping/'.Uuid::uuid());

    $response->assertStatus(200);
});

it('does not accept a ping without an UUID', function () {
    $response = $this->get('/ping');

    $response->assertStatus(400);
});

