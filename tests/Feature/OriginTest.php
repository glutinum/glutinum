<?php

use App\Models\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->runDatabaseMigrations();
});

it('has a protected origins page', function () {
    $response = $this->get('/origins');

    $response->assertStatus(302)
             ->assertRedirect('/login');
});

it('shows the origins page when logged in', function () {
    $response = $this->actingAs($this->user)
                     ->get('/origins');

    $response->assertStatus(200)
             ->assertSee('Repositories');
});
