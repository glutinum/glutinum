<?php
declare(strict_types=1);

use App\Exceptions\MissingKeyException;
use App\Http\Controllers\PingController;

uses()->group('ping');

it("throws an exception", function () {
    $controller = new PingController;
    $controller->check();
})->throws(MissingKeyException::class);
